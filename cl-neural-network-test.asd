(defsystem cl-neural-network
  :author "Hirotetsu Hongo <ekatsymeee@gmail.com>"
  :maintainer "Hirotetsu Hongo <ekatsymeee@gmail.com>"
  :license "MIT"
  :version "0.0.1"
  :homepage "https://gitlab.com/ekatsym/cl-neural-network"
  :description "A framework for neural network as CLOS objects."
  :depends-on (:alexandria
               :cl-num-utils
               :anaphora
               :mt19937
               :cl-cuda
               :mgl-mat
               :fiveam)
  :components ((:module "t"
                :serial t
                :components
                ((:file "util-test")
                 (:module "neural-layer"
                  :serial t
                  :components
                  ((:module "prelayer"
                    :serial t
                    :components
                    ((:file "affine-test")
                     (:file "activation-test")))
                   (:module "layers"
                    :serial t
                    :components
                    ((:file "layer-test")
                     (:file "layers-test")))))
                 (:module "neural-network"
                  :serial t
                  :components
                  ((:file "p-test")
                   (:file "ffn-test")
                   (:file "rbf-test")
                   (:file "svm-test")
                   (:file "cnn-test")
                   (:file "rnn-test")
                   (:file "lstm-test")
                   (:file "ae-test")))
                 (:file "package")))))

