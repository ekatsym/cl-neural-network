(defpackage cl-neural-network.neural-network.perceptron
  (:nicknames :nn.network.p)
  (:use :cl
        :cl-cuda
        :mgl-mat
        :nn.util
        :nn.config
        :nn.layer
        :nn.layer.pre.affine
        :nn.layer.pre.activation
        :nn.layer.input
        :nn.layer.hidden
        :nn.layer.output
        :nn.network
        )
  (:export )
  )

(in-package :nn.network.p)

