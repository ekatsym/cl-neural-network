(defpackage cl-neural-network.neural-network
  (:nicknames nn.network)
  (:use :cl
        :cl-cuda
        :mgl-mat
        :nn.util
        :nn.config
        :nn.layer.pre.affine
        :nn.layer.pre.activation
        :nn.layer
        :nn.layer.input
        :nn.layer.output
        :nn.layer.hidden
        )
  (:export #:hidden-layer-list
           #:neural-network
           #:make-neural-network
           #:forward
           #:backward
           #:loss
           #:neural-network-input-size
           #:neural-network-output-size
           #:neural-network-input-layer
           #:neural-network-output-layer
           #:neural-network-hidden-layers
           )
  )

(in-package :nn.network)

(defun hidden-layer-list? (object)
  (and (listp object)
       (every (lambda (x) (typep x 'hidden-layer))
              object)))

(deftype hidden-layer-list ()
  `(satisfies hidden-layer-list?))

(defclass-with-accessor-initarg-type neural-network ()
  (input-size integer)
  (output-size integer)
  (input-layer input-layer)
  (output-layer output-layer)
  (hidden-layers hidden-layer-list))

(defun make-neural-network (&rest layers)
  (cons-bind (o-layer rest-layers) (reverse layers)
    (cons-bind (i-layer h-layers) (reverse rest-layers)
      (make-instance 'neural-network
                     :input-size (neural-layer-input-size i-layer)
                     :output-size (neural-layer-output-size o-layer)
                     :input-layer i-layer
                     :output-layer o-layer
                     :hidden-layers h-layers))))

(defmethod forward ((object neural-network) (mat mat))
  (with-slots ((i-layer input-layer)
               (o-layer output-layer)
               (h-layers hidden-layers)) object
    (forward o-layer
             (reduce (lambda (mat obj) (forward obj mat)) h-layers
                     :initial-value (forward i-layer mat)))))

(defmethod backward ((object neural-network) (mat mat))
  (with-slots ((i-layer input-layer)
               (o-layer output-layer)
               (h-layers hidden-layers)) object
    (backward i-layer
              (reduce #'backward h-layers
                      :from-end t
                      :initial-value (backward o-layer mat)))))

(defmethod update ((object neural-network) batch-size)
  (with-slots ((h-layers hidden-layers)) object
    (mapc #'update h-layers (make-list (length h-layers) :initial-element batch-size))))

(defmethod loss ((object neural-network))
  (with-slots ((o-layer output-layer)) object
    (loss o-layer)))
