(defpackage cl-neural-network.config
  (:nicknames :nn.config)
  (:use :cl :mgl-mat :nn.util)
  (:export #:*default-mat-ctype*
           #:forward
           #:backward
           #:loss
           #:update
           #:learn
           )
  )

(in-package :nn.config)

;;; mat
(setf *default-mat-ctype* :float)

;;; generic function of neural-layer and neural-network
(defgeneric forward (object mat)
  (:documentation
    "Run forward function of the CLOS objects (affine, activation, neural-layer or neural-network)."))

(defgeneric backward (object mat)
  (:documentation
    "Run backward function of the CLOS objects (affine, activation, neural-layer or neural-network)."))

(defgeneric loss (object)
  (:documentation
    "Run backward function of the CLOS objects (affine, activation, neural-layer or neural-network)."))

(defgeneric update (object batch-size)
  (:documentation
    "Run learn function of the CLOS objects (affine, activation, neural-layer or neural-network)."))
