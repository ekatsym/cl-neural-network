(defpackage cl-neural-network.dataset
  (:nicknames nn.dataset)
  (:use :cl
        :cl-cuda
        :mgl-mat
        :nn.util
        :nn.config
        :nn.layer
        :nn.layer.output
        :nn.layer.pre.activation
        :nn.network
        )
  (:export #:dataset
           #:make-dataset
           #:dataset-size
           #:dataset-input-data
           #:dataset-output-data
           #:learn
           #:print-result
           )
  )

(in-package :nn.dataset)

(defstruct (dataset (:constructor make-dataset (input output)))
  (size (length input) :type fixnum)
  (input-data (coerce input 'vector)
              :type simple-vector)
  (output-data (coerce (subseq output 0 (length input)) 'vector)
               :type simple-vector))

(defun learn (times dataset neural-network batch-size)
  (dotimes (i times)
    (let ((batch-list (list-shuffle (iota batch-size))))
      (dolist (index batch-list)
        (let ((i-datum (svref (dataset-input-data dataset) index))
              (o-datum (svref (dataset-output-data dataset) index)))
          (forward neural-network i-datum)
          (backward neural-network o-datum)))
      (update neural-network batch-size))))

(defun print-result (dataset neural-network)
  (dotimes (i (dataset-size dataset))
    (forward neural-network (svref (dataset-input-data dataset) i))
    (backward neural-network (svref (dataset-output-data dataset) i))
    (format t "~%INPUT:~8t~a~%OUTPUT:~8t~a~%LOSS:~8t~a~%"
            (mat-to-array (svref (dataset-input-data dataset) i))
            (mat-to-array (activation-output
                            (output-layer-loss-activation
                              (neural-network-output-layer neural-network))))
            (loss neural-network))))
