(defpackage cl-neural-network.neural-layer.prelayer.activation
  (:nicknames :nn.layer.pre.activation)
  (:use :common-lisp :mgl-mat :nn.util :nn.config)
  (:import-from :alexandria
                #:symbolicate)
  (:export #:activation
           #:activation-size
           #:activation-output
           #:activation-dloss/dinput
           #:make-activation
           #:relu
           #:loss-activation
           #:loss-activation-size
           #:loss-activatino-output
           #:loss-activation-dloss/dinput
           #:loss-activation-target
           #:loss-activation-loss
           #:make-loss-activation
           #:softmax-cross-entropy
           #:make-softmax-cross-entropy
           #:forward
           #:backward
           #:loss
           ))

(in-package :nn.layer.pre.activation)

;;;; activation function
(defclass-with-accessor-initarg-type activation ()
  (size integer)
  (output mat)
  (dloss/dinput mat))

(defun make-activation (size activation-name)
  (make-instance activation-name
                 :size          size
                 :output        (make-mat (list size 1))
                 :dloss/dinput  (make-mat (list 1 size))))

;;; relu
(defclass relu (activation) ())

(defmethod forward ((object relu) (mat mat))
  (with-slots
    ((size size)
     (o output))
    object
    (copy! mat o
           :n size)
    (.max! 0.0 o)))

(defmethod backward ((object relu) (mat mat))
  (with-slots
    ((size size)
     (o output)
     (dl/di dloss/dinput))
    object
    (copy! (transpose mat) dl/di
           :n size)
    (dotimes (i size)
      (when (zerop (mref o i 0))
        (setf (mref dl/di 0 i) 0)))
    dl/di))



;;;; loss function
(defclass-with-accessor-initarg-type loss-activation (activation)
  (target mat)
  (loss float))

(defun make-loss-activation (size loss-activation-name)
  (make-instance loss-activation-name
                 :size          size
                 :output        (make-mat (list size 1))
                 :dloss/dinput  (make-mat (list 1 size))
                 :target        (make-mat (list size 1))
                 :loss          0.0))

;;; softmax-cross-entropy
(defclass softmax-cross-entropy (loss-activation) ())

(defmethod forward ((object softmax-cross-entropy) (mat mat))
  (with-slots ((size size)
               (out output)) object
    (copy! mat out
           :n size)
    (.exp! out)
    (scal! (/ 1.0 (asum out)) out)))

(defmethod backward ((object softmax-cross-entropy) (mat mat))
  (with-slots ((size size)
               (out output)
               (tgt target)
               (dl/di dloss/dinput)) object
    (copy! mat tgt
           :n size)
    (setf dl/di (transpose (m- out tgt)))))

(defmethod loss ((object softmax-cross-entropy))
  (with-slots ((size size)
               (out output)
               (tgt target)
               (loss loss)) object
    (setf loss (- (dot (.log! (copy-mat out)
                              :n size)
                       tgt
                       :n size)))))

(defclass root-mean-square (loss-activation) ())

(defmethod forward ((object root-mean-square) (mat mat))
  (with-slots ((size size)
               (out output)) object
    (copy! mat out
           :n size)))

(defmethod backward ((object root-mean-square) (mat mat))
  (with-slots ((size size)
               (out output)
               (tgt target)
               (dl/di dloss/dinput)) object
    (copy! mat tgt
           :n size)
    (setf dl/di (transpose (m- out tgt)))))

(defmethod loss ((object root-mean-square))
  (with-slots ((size size)
               (out output)
               (tgt target)) object
    (/ (nrm2 (m- tgt out)
             :n size)
       2)))
