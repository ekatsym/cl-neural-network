(defpackage cl-neural-network.neural-layer.prelayer.affine
  (:nicknames :nn.layer.pre.affine)
  (:use :common-lisp :mgl-mat :nn.util :nn.config)
  (:export #:affine
           #:affine-weight
           #:affine-bias
           #:affine-input-size
           #:affine-output-size
           #:affine-input
           #:affine-output
           #:affine-dloss/dinput
           #:affine-dloss/dweight
           #:affine-dloss/dbias
           #:affine-learning-rate
           #:make-affine
           #:forward
           #:backward
           #:update))

(in-package :nn.layer.pre.affine)

(defclass-with-accessor-initarg-type affine ()
  (weight mat)
  (bias mat)
  (input-size integer)
  (output-size integer)
  (input mat)
  (output mat)
  (dloss/dinput mat)
  (dloss/dweight mat)
  (dloss/dbias mat)
  (learning-rate float))

(defun make-affine (input-size output-size learning-rate)
  (make-instance 'affine
                 :weight        (make-random-mat (list output-size input-size))
                 :bias          (make-random-mat (list output-size 1))
                 :input-size    input-size
                 :output-size   output-size
                 :input         (make-mat (list input-size 1))
                 :output        (make-mat (list output-size 1))
                 :dloss/dinput  (make-mat (list 1 input-size))
                 :dloss/dweight (make-mat (list input-size output-size))
                 :dloss/dbias   (make-mat (list 1 output-size))
                 :learning-rate learning-rate))

(defmethod forward ((object affine) (mat mat))
  (with-slots ((w weight)
               (b bias)
               (i-size input-size)
               (o-size output-size)
               (i input)
               (o output)) object
    (copy! mat i
           :n i-size)
    (gemm! 1.0 w i 0.0 o                  ; o <- Wi
           :m o-size :n 1 :k i-size)
    (axpy! 1.0 b o                        ; o <- b + o (= b + Wi)
           :n o-size)))

(defmethod backward ((object affine) (mat mat))
  (with-slots ((w weight)
               (i input)
               (i-size input-size)
               (o-size output-size)
               (dl/di dloss/dinput)
               (dl/dw dloss/dweight)
               (dl/db dloss/dbias)) object
    (axpy! 1.0 mat dl/db                  ; dL/db <- dL/do + dL/db
           :n o-size)
    (gemm! 1.0 i mat 1.0 dl/dw            ; dL/dW <- i * dL/do + dL/dW
           :m i-size :n o-size :k 1)
    (gemm! 1.0 mat w 0.0 dl/di            ; dL/di <- dL/do * W
           :m 1 :n i-size :k o-size)))

(defmethod update ((object affine) batch-size)
  (with-slots ((w weight)
               (b bias)
               (i input)
               (o output)
               (i-size input-size)
               (o-size output-size)
               (dl/dw dloss/dweight)
               (dl/db dloss/dbias)
               (rate learning-rate)) object
    (axpy! (- (/ rate batch-size)) (transpose dl/dw) w ; w <- -e/N * dL/dW^t + w
           :n (* i-size o-size))
    (axpy! (- (/ rate batch-size)) (transpose dl/db) b ; b <- -e/N * dL/db^t + b
           :n o-size)
    (fill! 0.0 dl/dw
           :n (* i-size o-size))
    (fill! 0.0 dl/db
           :n o-size)
    (fill! 0.0 i)
    (fill! 0.0 o)
    t))
