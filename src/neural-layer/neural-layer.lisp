(defpackage cl-neural-network.neural-layer
  (:nicknames nn.layer)
  (:use :cl
        :mgl-mat
        :nn.util
        :nn.config
        :nn.layer.pre.affine
        :nn.layer.pre.activation)
  (:export #:neural-layer
           #:neural-layer-input-size
           #:neural-layer-output-size))

(in-package :nn.layer)

(defclass-with-accessor-initarg-type neural-layer ()
  (input-size integer)
  (output-size integer))
