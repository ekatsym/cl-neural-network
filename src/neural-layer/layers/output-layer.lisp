(defpackage cl-neural-network.neural-layer.output-layer
  (:nicknames nn.layer.output)
  (:use :cl
        :mgl-mat
        :nn.util
        :nn.config
        :nn.layer.pre.affine
        :nn.layer.pre.activation
        :nn.layer)
  (:export #:output-layer
           #:output-layer-loss-activation
           #:make-output-layer
           #:forward
           #:backward
           #:loss))

(in-package :nn.layer.output)

(defclass-with-accessor-initarg-type output-layer (neural-layer)
  (loss-activation loss-activation))

(defun make-output-layer (size loss-activation-name)
  (make-instance 'output-layer
                 :input-size size
                 :output-size size
                 :loss-activation (make-loss-activation size loss-activation-name)))

(defmethod forward ((object output-layer) (mat mat))
  (with-slots ((l-act loss-activation)) object
    (forward l-act mat)))

(defmethod backward ((object output-layer) (mat mat))
  (with-slots ((l-act loss-activation)) object
    (backward l-act mat)))

(defmethod loss ((object output-layer))
  (with-slots ((l-act loss-activation)) object
    (loss l-act)))
