(defpackage cl-neural-network.neural-layer.input-layer
  (:nicknames nn.layer.input)
  (:use :cl
        :mgl-mat
        :nn.util
        :nn.config
        :nn.layer.pre.affine
        :nn.layer.pre.activation
        :nn.layer)
  (:export #:input-layer
           #:make-input-layer
           #:forward
           #:backward
           #:update))

(in-package :nn.layer.input)

(defclass-with-accessor-initarg-type input-layer (neural-layer))

(defun make-input-layer (size)
  (make-instance 'input-layer
                 :input-size size
                 :output-size size))

(defmethod forward ((object input-layer) (mat mat))
  mat)

(defmethod backward ((object input-layer) (mat mat))
  mat)
