(defpackage cl-neural-network.neural-layer.hidden-layer
  (:nicknames nn.layer.hidden)
  (:use :cl
        :mgl-mat
        :nn.util
        :nn.config
        :nn.layer.pre.affine
        :nn.layer.pre.activation
        :nn.layer)
  (:export #:hidden-layer
           #:hidden-layer-affine
           #:hidden-layer-activation
           #:make-hidden-layer))

(in-package :nn.layer.hidden)

(defclass-with-accessor-initarg-type hidden-layer (neural-layer)
  (affine affine)
  (activation activation))

(defun make-hidden-layer (input-size output-size activation-name learning-rate)
  (make-instance 'hidden-layer
                 :input-size input-size
                 :output-size output-size
                 :affine (make-affine input-size output-size learning-rate)
                 :activation (make-activation output-size activation-name)))

(defmethod forward ((object hidden-layer) (mat mat))
  (with-slots ((affine affine) (activation activation)) object
    (forward activation (forward affine mat))))

(defmethod backward ((object hidden-layer) (mat mat))
  (with-slots ((affine affine) (activation activation)) object
    (backward affine (backward activation mat))))

(defmethod loss ((object hidden-layer))
  (with-slots ((activation activation)) object
    (loss activation)))

(defmethod update ((object hidden-layer) batch-size)
  (with-slots ((affine affine)) object
    (update affine batch-size)))
