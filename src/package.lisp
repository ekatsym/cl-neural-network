(defpackage cl-neural-network
  (:nicknames nn)
  (:use :cl
        :nn.util
        :nn.config
        :nn.layer.pre.affine
        :nn.layer.pre.activation
        :nn.layer.input
        :nn.layer.output
        :nn.layer.hidden
        :nn.network
        :nn.dataset
        )
  (:export #:affine
           #:make-affine
           #:activation
           #:make-activation
           #:relu
           #:softmax-cross-entropy
           #:neural-layer
           #:make-input-layer
           #:make-output-layer
           #:make-hidden-layer
           #:neural-network
           #:make-neural-network
           #:forward
           #:backward
           #:update
           #:loss
           #:learn
           )
  )

(in-package :nn)

;(defparameter *xor-nn*
;  (make-neural-network
;    (make-input-layer 2)
;    (make-hidden-layer 2 10 'relu 1.0e-5)
;    (make-hidden-layer 10 2 'relu 1.0e-5)
;    (make-output-layer 2 'softmax-cross-entropy)))
;
;(defparameter *xor-dataset*
;  (make-dataset
;    (mapcar (lambda (lst)
;              (mgl-mat:make-mat (list 2 1)
;                                :initial-contents lst))
;            (list (list (list 0 0))
;                  (list (list 0 1))
;                  (list (list 1 0))
;                  (list (list 1 1))))
;    (mapcar (lambda (lst)
;              (mgl-mat:make-mat (list 2 1)
;                                :initial-contents lst))
;            (list (list (list 1 0))
;                  (list (list 0 1))
;                  (list (list 0 1))
;                  (list (list 1 0))))))
;
;(print-result *xor-dataset* *xor-nn*)
;(time (progn
;        (learn 10000 *xor-dataset* *xor-nn* 1)
;        (print-result *xor-dataset* *xor-nn*)))
