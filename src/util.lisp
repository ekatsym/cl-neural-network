(defpackage cl-neural-network.util
  (:nicknames :nn.util)
  (:use :common-lisp :mgl-mat)
  (:import-from :alexandria
                #:with-gensyms
                #:symbolicate)
  (:export #:cons-bind
           #:list-bind
           #:vector-bind
           #:array-bind
           #:hash-table-bind
           #:slot-bind
           #:iota
           #:build-list
           #:remove-nth
           #:random-nth
           #:list-shuffle
           #:defclass-with-accessor-initarg-type
           #:make-random-mat
           #:class-bind
           )
  )

(in-package :nn.util)

;;; bind
(defmacro cons-bind ((car-var cdr-var) cons &body body)
  (with-gensyms (variable)
    `(let ((,variable ,cons))
       (let ((,car-var (car ,variable))
             (,cdr-var (cdr ,variable)))
         ,@body))))

(defmacro list-bind (list-vars list &body body)
  `(destructuring-bind ,list-vars ,list ,@body))

(defmacro vector-bind (vector-vars vector &body body)
  (with-gensyms (variable)
    `(let ((,variable ,vector))
       (let (,@(loop for i below (length vector)
                     for var in vector-vars
                     collect `(,var (svref ,variable ,i))))
         ,@body))))

(defmacro array-bind (array-bindings array &body body)
  (with-gensyms (variable)
    `(let ((,variable ,array))
       (let (,@(mapcar (lambda (array-binding)
                         (list-bind (array-var &rest subscripts) array-binding
                           `(,array-var (aref ,variable ,@subscripts))))
                       array-bindings))
         ,@body))))

(defmacro hash-table-bind (key-bindings hash-table &body body)
  (with-gensyms (variable)
    `(let ((,variable ,hash-table))
       (let (,@(mapcar (lambda (key-binding)
                         (list-bind (key-var key-name) key-binding
                           `(,key-var (gethash ',key-name ,variable))))
                       key-bindings))
         ,@body))))

(defmacro slot-bind (slot-bindings object &body body)
  (with-gensyms (variable)
    `(let ((,variable ,object))
       (let (,@(mapcar (lambda (slot-binding)
                         (list-bind (slot-var slot-name) slot-binding
                           `(,slot-var (slot-value ,variable ',slot-name))))
                       slot-bindings))
         ,@body))))


;;; list
(defun iota (size &optional (start 0) (step 1))
  (labels ((rec (i v acc)
             (if (zerop i)
                 acc
                 (rec (1- i) (+ v step) (cons v acc)))))
    (rec size start nil)))

(defun build-list (size &optional (function #'values))
  (labels ((rec (i acc)
             (if (zerop i)
                 acc
                 (rec (1- i) (cons (funcall function (1- i))
                                   acc)))))
    (rec size nil)))

(defun random-nth (list)
  (nth (random (length list)) list))

(defun remove-nth (n list)
  (labels ((rec (n lst acc)
             (if (zerop n)
                 (nconc (nreverse acc) (cdr lst))
                 (rec (1- n) (cdr lst) (cons (car lst) acc)))))
    (rec n list nil)))

(defun list-shuffle (list)
  (labels ((rec (lst acc)
             (if (null lst)
                 acc
                 (let ((n (random (length lst))))
                   (rec (remove-nth n lst) (cons (nth n lst) acc))))))
    (rec list nil)))


;;; define
(defun butmember (item list)
  (declare (optimize speed))
  (labels ((rec (lst acc)
             (if (null lst)
                 acc
                 (cons-bind (hd tl) lst
                   (if (equal item hd)
                       acc
                       (rec tl (cons hd acc)))))))
    (nreverse (rec list nil))))

(defmacro defclass-with-accessor-initarg-type (name direct-superclasses &body body)
  `(defclass ,name (,@direct-superclasses)
     ,(mapcar (lambda (slot)
                (let ((slot (if (listp slot) slot (list slot))))
                  (destructuring-bind (slot-name slot-type &rest slot-options) slot
                    `(,slot-name :accessor ,(symbolicate name '- slot-name)
                                 :initarg ,(intern (string slot-name) :keyword)
                                 :type ,slot-type
                                 ,@slot-options))))
              (butmember :options body))
     ,@(cdr (member :options body))))

(defmacro define-condition-with-accessor-initarg-type (name direct-superconditions slot-specs &body options)
  `(define-condition ,name (,@direct-superconditions)
     ,(mapcar (lambda (slot)
                (let ((slot (if (listp slot) slot (list slot))))
                  (destructuring-bind (slot-name slot-type &rest slot-options) slot
                    `(,slot-name :accessor ,(symbolicate name '- slot-name)
                                 :initarg ,(intern (string slot-name) :keyword)
                                 :type ,slot-type
                                 ,@slot-options))))
              slot-specs)
     ,@options))

;;; mat
(defun make-random-mat (dimensions &key (mean 0) (stddev 1))
  (let ((m (make-mat dimensions)))
    (gaussian-random! m :mean mean :stddev stddev)))

