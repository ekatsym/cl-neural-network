;(in-package :cl-user)
;
;(defpackage cl-neural-network-test.layer.pre.affine-test
;  (:use :cl :nn.layer.pre.affine :mgl-mat)
;  )

(in-package :nn.layer.pre.affine)

(defparameter *and-affine*
  (make-affine 2 1))

(forward *and-affine* (make-mat '(2 1)))
