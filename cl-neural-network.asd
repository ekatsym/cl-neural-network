(defsystem cl-neural-network
  :author "Hirotetsu Hongo <ekatsymeee@gmail.com>"
  :maintainer "Hirotetsu Hongo <ekatsymeee@gmail.com>"
  :license "MIT"
  :version "0.0.1"
  :homepage "https://gitlab.com/ekatsym/cl-neural-network"
  :description "A framework for neural network as CLOS objects."
  :depends-on (:alexandria
               :cl-num-utils
               :anaphora
               :mt19937
               :cl-cuda
               :mgl-mat)
  :components ((:module "src"
                :serial t
                :components
                ((:file "util")
                 (:file "config")
                 (:module "neural-layer"
                  :serial t
                  :components
                  ((:module "prelayer"
                    :serial t
                    :components
                    ((:file "affine")
                     (:file "activation")))
                   (:file "neural-layer")
                   (:module "layers"
                    :serial t
                    :components
                    ((:file "input-layer")
                     (:file "output-layer")
                     (:file "hidden-layer")
                     (:file "probabilistic-hidden-layer")
                     (:file "convolutional-layer")
                     (:file "pool-layer")
                     (:file "recurrent-layer")))))
                 (:module "neural-network"
                  :serial t
                  :components
                  ((:file "neural-network")
                   (:file "p")
                   (:file "ffn")
                   (:file "rbf")
                   (:file "svm")  
                   (:file "cnn")
                   (:file "rnn")
                   (:file "lstm")
                   (:file "ae")))
                 (:file "dataset")
                 (:file "package")))))
